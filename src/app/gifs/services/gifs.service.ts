import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GifSearch, Gif } from '../interfaces/gif.interface';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private apiKey: string = "oey8TlErP7JSZP7eOXZKBbflL6pjAe1L";
  private url: string = "https://api.giphy.com/v1/gifs";
  private _searchHistory: string[] = [];

  public results: Gif[] = [];

  get searchHistory(){
    return [...this._searchHistory];
  }

  constructor(private http: HttpClient){

    this._searchHistory = JSON.parse(localStorage.getItem('record')!) || [];
    this.results = JSON.parse(localStorage.getItem('results')!) || [];
    
    // if (localStorage.getItem('record')) {
    //   this._searchHistory = JSON.parse(localStorage.getItem('record')!);
    // }
  }
  
  searchGifs(query:string){

    query = query.trim().toLowerCase();

    if (query.trim() === "") {
      return;
    }

    if (!this._searchHistory.includes(query)) {
      this._searchHistory.unshift(query);
      this._searchHistory = this._searchHistory.splice(0,10);
      
      localStorage.setItem('record', JSON.stringify(this._searchHistory));
    }

    const params = new HttpParams()
      .set('api_key', this.apiKey)
      .set('limit', '10')
      .set('q', query);


    this.http.get<GifSearch>(`${this.url}/search`, { params })
      .subscribe( res => {
        this.results = res.data;
        localStorage.setItem('results',  JSON.stringify(this.results));
      })


  }
}
